import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'auth.Authenticate.performLoginProfile'()

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/a_Edit Profile'))

WebUI.navigateToUrl('https://demo-app.online/dashboard/profile/edit')

WebUI.setText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/input_Fullname_name'), 'Muhammad reza wahyu adinata Alfaridzi')

WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/button_Save Changes'))

WebUI.click(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/strong_The name may not be greater than 30 _ae5806'))

WebUI.verifyElementVisible(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/strong_The name may not be greater than 30 _ae5806'),
	FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Object Repository/or1_Website/orw3_updateProfile/Page_Coding.ID - Dashboard/strong_The name may not be greater than 30 _ae5806'),
	'The name may not be greater than 30 characters.')

WebUI.closeBrowser()

