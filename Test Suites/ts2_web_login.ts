<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ts2_web_login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>682ff435-c12f-42d8-8a14-eacfc60ca8be</testSuiteGuid>
   <testCaseLink>
      <guid>7ab79c22-9c5c-4a65-aaf0-3c4240b5a4bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl10_Melakukan lupa kata sandi pada menu reset password dengan kendala waktu dengan cara merefresh</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>925295f5-b04a-429e-953c-d5f466f02279</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl1_Login dengan password yang kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>274fadde-d986-4925-8732-be9c5ce864a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl2_Login dengan email yang kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fa26ca3c-084a-4527-b3c6-7c58caccde57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl3_Login menggunakan data email yang valid dan password yang tidak valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0eb48e4c-8258-45b3-b497-af9182b52125</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl4_Login menggunakan data email yang tidak valid dan password yang tidak valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0c14f8bc-1f25-4fb1-ab9a-e5cab4580a46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl5_Login menggunakan data User ID valid dan Password yang valid</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4e474076-872b-4db6-b01e-e5a09bca616e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Web Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>4e474076-872b-4db6-b01e-e5a09bca616e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>fcad83c8-ded0-4a36-b792-687ead475a02</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4e474076-872b-4db6-b01e-e5a09bca616e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>bf7db822-3bd0-4299-b15b-8a7d509f760b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bfa87c69-f26b-4093-84b9-7bb1932c9db5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl6_Login menggunakan data kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f783caa1-92b5-4c8f-b8a5-33354fee2808</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl7_Mengecek Tombol Lupa Kata Sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3897bb38-8b15-4bbc-a222-98bada1e380e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl8_Lupa kata sandi menggunakan email yang tidak valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f8558b-9ade-4c32-8d2f-856d16fd8b0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/tc1_Website/w2_login/wl9_Lupa kata sandi menggunakan email yang valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
