<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Back to Events                 Day 4 Wo_057e68</name>
   <tag></tag>
   <elementGuidId>24a07f9e-071b-42f5-a926-f72fa45cb51c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5b015913-122f-4d58-af62-a8acca406134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-main-section</value>
      <webElementGuid>1a6b6f1a-2a76-4a21-be1e-5213816334d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

    
        
            
                Back to Events 
                Day 4: Workshop
            
        
        


            

                

                
                    Day 4: Workshop

                    
                    Hari Keempat kamu akan belajar prakter dan ada tes mengerjakan case juga loh! Pastinya asik dan seru banget bisa jadi portofolio kamu juga serta nanti ada penilaian dan sertifikat kelulusan!
Yukk belajar di Mini Class Coding.ID! Topik pertama  kamu akan belajar “Introduction to Python for Data Scientist” bersama dengan  Kak Ziyad Syauqi sebagai Data Scientist di Market Leader Company in Automotive Industry.


  
    
  

Investasi skill kamu hanya dengan Rp 85K 

  Jumat, 25 November 2022
  19.30-21.30 WIB Via Zoom


  


  
    Apa yang dibahas:
  


  Day 4: Workshop



  Peserta akan di challenge dengan sebuah case.
  Peserta yang memenuhi standar score yang ditentukan akan diberikan Sertifikat Kelulusan


  Kamu bisa beli Bundling atau Kelas Satuan.
  Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days


  
    Direkomendasikan untuk kamu yang:
  


  Punya basic mengenai Data Science
  Tertarik di bidang Data Science
  Tertarik di bidang Machine Learning
  Ingin menjadi Data Scientist
  Ingin menjadi Machine Learning Engineer

Kamu mendapat:

  Gratis E-Certificate
  Gratis materi mini class
  Gratis rekaman selama kelas
  Gratis konsultasi dengan praktisi
  Masuk grup whatsapp untuk diskusi bareng praktisi
  Portofolio Project
  Sertifikat Kelulusan

*Syarat dan ketentuan berlaku
SYARAT &amp; KETENTUAN

  Peserta akan diberikan link zoom lewat whatsapp dan email.
  Link Zoom akan dikirimkan via WA H-1 sampai maksimal 2 jam sebelum kelas dimulai
  Di akhir kelas akan dibagikan absensi untuk bukti kehadiran dan pengiriman materi, record video dan sertifikat (harap menulis nama lengkap dan email yang valid untuk kebutuhan sertifikat)
  Kelas akan diadakan dalam bahasa Indonesia


  

Website: www.coding.id Instagram: coding_id Telegram: https://tiny.cc/CodingIDtelegram Youtube: Coding id 
                
                


                
                

                
            
            

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 20  Menit  12 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            


        
        
            Event lain yang banyak diminati
        
        

            
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 3: Predict using Machine Learning
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                18 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 3: Predict using Machine Learning
                                        
                                    
                                    
                                        18 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 2: Data Wrangling with Python
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 2: Data Wrangling with Python
                                        
                                    
                                    
                                        11 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 1: Introduction to Python for Data Scientist
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 1: Introduction to Python for Data Scientist
                                        
                                    
                                    
                                        04 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                            
        


        
            Ikutan Program CODING.ID dijamin dapat kerja!
            

                
                
                

                    
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Quality Assurance Engineer Class
                                            
                                        
                                        
                                            Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Fullstack Engineer Class
                                            
                                        
                                        
                                            Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja ...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                        
                    
                

                

            
        


    

    
        
            
                
            
            
                
                    
                        Jadi Pembicara di Event Coding.id
                    
                    Berbagi ilmu dan skill yang kamu punya dengan teman-teman, dapatkan
                        berbagai keuntungan
                    


                    
                        Cari
                            Tahu Caranya
                        
                    
                
            
        
    
</value>
      <webElementGuid>22f94613-889b-4c6f-8448-3e7530107264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]</value>
      <webElementGuid>77c32eca-5299-492f-9c86-9b1b2d6550a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]</value>
      <webElementGuid>2c3bb956-b211-4e1c-af2a-085b4780c8e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

    
        
            
                Back to Events 
                Day 4: Workshop
            
        
        


            

                

                
                    Day 4: Workshop

                    
                    Hari Keempat kamu akan belajar prakter dan ada tes mengerjakan case juga loh! Pastinya asik dan seru banget bisa jadi portofolio kamu juga serta nanti ada penilaian dan sertifikat kelulusan!
Yukk belajar di Mini Class Coding.ID! Topik pertama  kamu akan belajar “Introduction to Python for Data Scientist” bersama dengan  Kak Ziyad Syauqi sebagai Data Scientist di Market Leader Company in Automotive Industry.


  
    
  

Investasi skill kamu hanya dengan Rp 85K 

  Jumat, 25 November 2022
  19.30-21.30 WIB Via Zoom


  


  
    Apa yang dibahas:
  


  Day 4: Workshop



  Peserta akan di challenge dengan sebuah case.
  Peserta yang memenuhi standar score yang ditentukan akan diberikan Sertifikat Kelulusan


  Kamu bisa beli Bundling atau Kelas Satuan.
  Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days


  
    Direkomendasikan untuk kamu yang:
  


  Punya basic mengenai Data Science
  Tertarik di bidang Data Science
  Tertarik di bidang Machine Learning
  Ingin menjadi Data Scientist
  Ingin menjadi Machine Learning Engineer

Kamu mendapat:

  Gratis E-Certificate
  Gratis materi mini class
  Gratis rekaman selama kelas
  Gratis konsultasi dengan praktisi
  Masuk grup whatsapp untuk diskusi bareng praktisi
  Portofolio Project
  Sertifikat Kelulusan

*Syarat dan ketentuan berlaku
SYARAT &amp; KETENTUAN

  Peserta akan diberikan link zoom lewat whatsapp dan email.
  Link Zoom akan dikirimkan via WA H-1 sampai maksimal 2 jam sebelum kelas dimulai
  Di akhir kelas akan dibagikan absensi untuk bukti kehadiran dan pengiriman materi, record video dan sertifikat (harap menulis nama lengkap dan email yang valid untuk kebutuhan sertifikat)
  Kelas akan diadakan dalam bahasa Indonesia


  

Website: www.coding.id Instagram: coding_id Telegram: https://tiny.cc/CodingIDtelegram Youtube: Coding id 
                
                


                
                

                
            
            

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 20  Menit  12 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            


        
        
            Event lain yang banyak diminati
        
        

            
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 3: Predict using Machine Learning
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                18 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 3: Predict using Machine Learning
                                        
                                    
                                    
                                        18 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 2: Data Wrangling with Python
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 2: Data Wrangling with Python
                                        
                                    
                                    
                                        11 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 1: Introduction to Python for Data Scientist
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 1: Introduction to Python for Data Scientist
                                        
                                    
                                    
                                        04 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                            
        


        
            Ikutan Program CODING.ID dijamin dapat kerja!
            

                
                
                

                    
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Quality Assurance Engineer Class
                                            
                                        
                                        
                                            Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Fullstack Engineer Class
                                            
                                        
                                        
                                            Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja ...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                        
                    
                

                

            
        


    

    
        
            
                
            
            
                
                    
                        Jadi Pembicara di Event Coding.id
                    
                    Berbagi ilmu dan skill yang kamu punya dengan teman-teman, dapatkan
                        berbagai keuntungan
                    


                    
                        Cari
                            Tahu Caranya
                        
                    
                
            
        
    
' or . = '

    
        
            
                Back to Events 
                Day 4: Workshop
            
        
        


            

                

                
                    Day 4: Workshop

                    
                    Hari Keempat kamu akan belajar prakter dan ada tes mengerjakan case juga loh! Pastinya asik dan seru banget bisa jadi portofolio kamu juga serta nanti ada penilaian dan sertifikat kelulusan!
Yukk belajar di Mini Class Coding.ID! Topik pertama  kamu akan belajar “Introduction to Python for Data Scientist” bersama dengan  Kak Ziyad Syauqi sebagai Data Scientist di Market Leader Company in Automotive Industry.


  
    
  

Investasi skill kamu hanya dengan Rp 85K 

  Jumat, 25 November 2022
  19.30-21.30 WIB Via Zoom


  


  
    Apa yang dibahas:
  


  Day 4: Workshop



  Peserta akan di challenge dengan sebuah case.
  Peserta yang memenuhi standar score yang ditentukan akan diberikan Sertifikat Kelulusan


  Kamu bisa beli Bundling atau Kelas Satuan.
  Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days


  
    Direkomendasikan untuk kamu yang:
  


  Punya basic mengenai Data Science
  Tertarik di bidang Data Science
  Tertarik di bidang Machine Learning
  Ingin menjadi Data Scientist
  Ingin menjadi Machine Learning Engineer

Kamu mendapat:

  Gratis E-Certificate
  Gratis materi mini class
  Gratis rekaman selama kelas
  Gratis konsultasi dengan praktisi
  Masuk grup whatsapp untuk diskusi bareng praktisi
  Portofolio Project
  Sertifikat Kelulusan

*Syarat dan ketentuan berlaku
SYARAT &amp; KETENTUAN

  Peserta akan diberikan link zoom lewat whatsapp dan email.
  Link Zoom akan dikirimkan via WA H-1 sampai maksimal 2 jam sebelum kelas dimulai
  Di akhir kelas akan dibagikan absensi untuk bukti kehadiran dan pengiriman materi, record video dan sertifikat (harap menulis nama lengkap dan email yang valid untuk kebutuhan sertifikat)
  Kelas akan diadakan dalam bahasa Indonesia


  

Website: www.coding.id Instagram: coding_id Telegram: https://tiny.cc/CodingIDtelegram Youtube: Coding id 
                
                


                
                

                
            
            

                
                    
                        
                                                            
                                                    

                        
                            Coach
                            Ziyad Syauqi Fawwazi
                            
                            
                                Data Scientist Market Leader Company in Automotive Industry
                            
                        
                    
                
                
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 20  Menit  12 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                

            


        
        
            Event lain yang banyak diminati
        
        

            
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 3: Predict using Machine Learning
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                18 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 3: Predict using Machine Learning
                                        
                                    
                                    
                                        18 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 2: Data Wrangling with Python
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 2: Data Wrangling with Python
                                        
                                    
                                    
                                        11 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                                    
                        
                            
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 1: Introduction to Python for Data Scientist
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            CLOSE
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 1: Introduction to Python for Data Scientist
                                        
                                    
                                    
                                        04 Nov 2022 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            
                        
                    
                            
        


        
            Ikutan Program CODING.ID dijamin dapat kerja!
            

                
                
                

                    
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Quality Assurance Engineer Class
                                            
                                        
                                        
                                            Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                                                    

                                
                                    
                                        
                                            
                                        
                                    
                                    

                                        
                                            Fullstack Engineer Class
                                            
                                        
                                        
                                            Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja ...

                                        
                                            
                                                Lihat Program 
                                            
                                        
                                    
                                

                            
                        
                    
                

                

            
        


    

    
        
            
                
            
            
                
                    
                        Jadi Pembicara di Event Coding.id
                    
                    Berbagi ilmu dan skill yang kamu punya dengan teman-teman, dapatkan
                        berbagai keuntungan
                    


                    
                        Cari
                            Tahu Caranya
                        
                    
                
            
        
    
')]</value>
      <webElementGuid>9dcbb730-f3a9-4cad-9d3d-4e641d38dd70</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
