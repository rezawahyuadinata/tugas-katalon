<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_My                                      _41f013</name>
   <tag></tag>
   <elementGuidId>084de83c-3a00-4509-b48f-110858a368ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>2770f21b-401b-4b3c-af8e-0215ab329390</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>___class_+?26___</value>
      <webElementGuid>95eb5717-5772-48c0-a941-eb278caaee82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                </value>
      <webElementGuid>115dbd91-6b37-4efd-830d-be483623c959</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]</value>
      <webElementGuid>b99b7312-088e-4105-a6c6-f787becd9430</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      <webElementGuid>f91f485f-1466-46ff-bdc9-58e629560cbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::li[1]</value>
      <webElementGuid>d5b48409-d23f-4262-a4fd-bd10448f1852</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::li[2]</value>
      <webElementGuid>df42959c-5a6e-4a41-8af2-22abb7c72e71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]</value>
      <webElementGuid>eaa8c81e-9af9-468a-ac1a-c7a1cdba1565</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ' or . = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ')]</value>
      <webElementGuid>651742b2-7d21-4b91-bb83-0c492aaca04a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
