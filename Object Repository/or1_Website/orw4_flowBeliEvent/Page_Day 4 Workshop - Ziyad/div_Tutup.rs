<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Tutup</name>
   <tag></tag>
   <elementGuidId>0ce0d54f-9a26-4331-8a96-7eb2b06e0290</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8cd018e3-a190-433b-beb7-4763dcb352d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>6767879e-56ea-4049-baef-4e208c9cff9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
             Tutup 
            
            

        </value>
      <webElementGuid>2c83d5d6-cbd5-46ee-844b-88a723a41691</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>839f10d0-52fb-43c1-bfeb-4624664194d8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div</value>
      <webElementGuid>5cdc527e-d6a7-4f07-89e5-7f6d50ef5a93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Pembicara di Event Coding.id'])[1]/following::div[3]</value>
      <webElementGuid>5da684d8-79d2-4830-b5f4-e842b6745560</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event already on Cart'])[1]/preceding::div[1]</value>
      <webElementGuid>6bcb1b70-dbaf-47d0-b11e-165a53ba4df0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div</value>
      <webElementGuid>3abb00b2-5927-46bf-83da-a5f64ecfd45b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
             Tutup 
            
            

        ' or . = '
            
             Tutup 
            
            

        ')]</value>
      <webElementGuid>fb45da04-4ca8-4a6a-8414-883901dd0cce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
