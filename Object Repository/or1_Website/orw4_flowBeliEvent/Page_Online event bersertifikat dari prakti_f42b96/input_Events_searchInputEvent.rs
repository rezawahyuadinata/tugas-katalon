<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Events_searchInputEvent</name>
   <tag></tag>
   <elementGuidId>5e7828dc-0d47-438d-850a-1c76c5c5cbd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='searchInputEvent']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#searchInputEvent</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>128d1ff2-3d3b-4c3d-8651-bf18926a2aa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchInputEvent</value>
      <webElementGuid>c70c6769-d6dd-4573-b018-5e53d7bc674c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:model</name>
      <type>Main</type>
      <value>search</value>
      <webElementGuid>cf833e0b-f1b5-42b3-a134-414c49816446</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>6fef21f8-5217-4c0b-96ad-d8a680926adf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control searchinput</value>
      <webElementGuid>3cf9901b-9e40-4858-b09e-03f2407cd9ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value></value>
      <webElementGuid>5e923438-ad81-4b87-8cc6-fa204b0af6bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searchInputEvent&quot;)</value>
      <webElementGuid>3f860cf2-cd66-4a76-b5d8-6dad1e2191e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='searchInputEvent']</value>
      <webElementGuid>c832b38f-71d8-417b-8344-1cb4181eb2be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']/div/div/div[2]/input</value>
      <webElementGuid>a0d4a8a4-21c4-47d5-ac71-a3af5c7d4c21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>eca99b37-0dfe-448d-b20e-98d81d70ed14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'searchInputEvent' and @type = 'text' and @placeholder = '']</value>
      <webElementGuid>45d12590-8c71-4b47-ab4a-bff0027f9c9d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
