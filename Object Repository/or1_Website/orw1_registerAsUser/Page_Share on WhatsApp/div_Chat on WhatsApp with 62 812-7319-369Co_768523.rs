<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Chat on WhatsApp with 62 812-7319-369Co_768523</name>
   <tag></tag>
   <elementGuidId>8ba570df-e20e-4584-a2c2-d63ec7103bbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div._9vd6._9t33._9bir._9bj3._9bhj._9v12._9tau._9tay._9u6w._9se-._9u5y</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main_block']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5a5038f0-ac68-46ca-9a54-1725e5ed2d7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>_9vd6 _9t33 _9bir _9bj3 _9bhj _9v12 _9tau _9tay _9u6w _9se- _9u5y</value>
      <webElementGuid>7f59fa03-92a9-4f79-9d66-50dc2f982a25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Chat on WhatsApp with +62 812-7319-369Continue to Chat</value>
      <webElementGuid>add94880-94b3-4d8f-8b90-e169cb445cd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_block&quot;)/div[@class=&quot;_9vd6 _9t33 _9bir _9bj3 _9bhj _9v12 _9tau _9tay _9u6w _9se- _9u5y&quot;]</value>
      <webElementGuid>fe656f30-e623-41e0-91b9-0ac9e7e3041c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main_block']/div</value>
      <webElementGuid>6e4d2cd0-64cc-4b0e-a4a5-846cf2fef40c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[2]/following::div[8]</value>
      <webElementGuid>a185ae9a-d858-4270-9995-731dc8a535ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WhatsApp Web'])[1]/following::div[8]</value>
      <webElementGuid>b2ca4eb9-e2d6-4722-be97-fb27e5ffc4e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't have WhatsApp yet?')])[1]/preceding::div[1]</value>
      <webElementGuid>9ed74815-b892-45fa-9bff-b95e469d0016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/div</value>
      <webElementGuid>91dd813e-898f-477b-93b0-7c2cbad51317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Chat on WhatsApp with +62 812-7319-369Continue to Chat' or . = 'Chat on WhatsApp with +62 812-7319-369Continue to Chat')]</value>
      <webElementGuid>e52e90a7-903c-43bc-baf9-8575faf8ebdf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
