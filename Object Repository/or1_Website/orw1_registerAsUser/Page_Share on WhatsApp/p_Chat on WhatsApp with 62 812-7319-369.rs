<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Chat on WhatsApp with 62 812-7319-369</name>
   <tag></tag>
   <elementGuidId>adeddf81-70c4-46fb-8529-3ee8c90ebab3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2._9vd5._9scb > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main_block']/div/h2/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>41db6c2f-e030-4af5-8f24-187854f6d02e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Chat on WhatsApp with +62 812-7319-369</value>
      <webElementGuid>bad83532-7efc-4c8b-946a-a4603087f273</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main_block&quot;)/div[@class=&quot;_9vd6 _9t33 _9bir _9bj3 _9bhj _9v12 _9tau _9tay _9u6w _9se- _9u5y&quot;]/h2[@class=&quot;_9vd5 _9scb&quot;]/p[1]</value>
      <webElementGuid>cadff4f3-2ac1-4c2e-b782-37380ac37efc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main_block']/div/h2/p</value>
      <webElementGuid>d698abfb-87cd-43a8-8ba2-d82d2c9360b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[2]/following::p[1]</value>
      <webElementGuid>efe4bfa2-8c8d-4a66-ab0a-4e859c1876e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WhatsApp Web'])[1]/following::p[1]</value>
      <webElementGuid>9d9ec6a0-dd0c-4319-8db1-993c247cbd9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to Chat'])[1]/preceding::p[1]</value>
      <webElementGuid>59f08587-5e04-48f6-b553-344e398669d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Don', &quot;'&quot;, 't have WhatsApp yet?')])[1]/preceding::p[2]</value>
      <webElementGuid>411b76a3-d3b1-4334-8e8a-69bfde228b56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chat on WhatsApp with']/parent::*</value>
      <webElementGuid>cd7c0f13-3171-41fa-af8b-fc289184eb0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2/p</value>
      <webElementGuid>07b75805-e55a-451b-97b7-4ef712c3a486</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Chat on WhatsApp with +62 812-7319-369' or . = 'Chat on WhatsApp with +62 812-7319-369')]</value>
      <webElementGuid>38d69ee4-dba1-4a42-adda-64566f428181</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
