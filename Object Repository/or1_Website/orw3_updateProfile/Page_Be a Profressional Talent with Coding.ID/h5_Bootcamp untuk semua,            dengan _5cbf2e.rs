<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Bootcamp untuk semua,            dengan _5cbf2e</name>
   <tag></tag>
   <elementGuidId>210efc88-0102-4304-9aa2-1362ec78e3c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.new_body1_regular</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>9f191c8b-a557-402f-84d7-02d6b4301130</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_regular</value>
      <webElementGuid>7686de21-535e-4bab-afee-85a332dc8e76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.</value>
      <webElementGuid>325d1f6e-8a10-4c83-b491-b14228ac0345</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h5[@class=&quot;new_body1_regular&quot;]</value>
      <webElementGuid>ae27ea8e-4edb-4e8c-ad25-a71a77f1b1eb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h5</value>
      <webElementGuid>09bd26c9-b759-4a77-9975-b41bf0e1330e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h5[1]</value>
      <webElementGuid>71e2a8bd-9ec7-4f5f-84cc-70573f74fff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h5[2]</value>
      <webElementGuid>eb763348-b599-4141-9f4e-b8394e1e7e53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/h5</value>
      <webElementGuid>1dbfba0b-394f-4d7e-8b78-a4c4ed2b07c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.' or . = 'Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.')]</value>
      <webElementGuid>58605d87-c20e-4f25-a4ea-c5ba2d046b85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
